namespace InsuranceT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Employee")]
    public partial class Employee
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Employee()
        {
            PolicyRequests = new HashSet<PolicyRequest>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(250)]
        public string designation { get; set; }

        [Column(TypeName = "date")]
        public DateTime joindate { get; set; }

        public decimal? salary { get; set; }

        [StringLength(250)]
        public string firstname { get; set; }

        [StringLength(250)]
        public string lastname { get; set; }

        [StringLength(250)]
        public string username { get; set; }

        [StringLength(250)]
        public string password { get; set; }

        [StringLength(250)]
        public string address { get; set; }

        [StringLength(50)]
        public string contactno { get; set; }

        [StringLength(50)]
        public string state { get; set; }

        [StringLength(250)]
        public string country { get; set; }

        [StringLength(250)]
        public string city { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PolicyRequest> PolicyRequests { get; set; }
    }
}
