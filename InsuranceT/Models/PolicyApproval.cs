namespace InsuranceT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PolicyApproval")]
    public partial class PolicyApproval
    {
        public int Id { get; set; }

        public int PolicyRequestId { get; set; }

        [Column(TypeName = "date")]
        public DateTime Pstartdate { get; set; }

        [Column(TypeName = "date")]
        public DateTime Penddate { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(250)]
        public string Reason { get; set; }

        public virtual PolicyRequest PolicyRequest { get; set; }
    }
}
