namespace InsuranceT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Company")]
    public partial class Company
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Company()
        {
            policies = new HashSet<policy>();
        }

        public int Id { get; set; }

        [StringLength(50)]
        [DisplayName("Company Name :")]
        public string CompanyName { get; set; }

        [StringLength(50)]
        [DisplayName("Address :")]
        public string Address { get; set; }

        [StringLength(20)]
        [DisplayName("Phone no :")]
        public string Phone { get; set; }

        [StringLength(250)]
        [DisplayName("Company Url :")]
        public string CompanyURL { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<policy> policies { get; set; }
    }
}
