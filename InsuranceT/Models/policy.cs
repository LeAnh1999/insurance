namespace InsuranceT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class policy
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public policy()
        {
            PolicyRequests = new HashSet<PolicyRequest>();
        }

        public int Id { get; set; }

        [StringLength(250)]
        [DisplayName("Policy Type Name :")]
        public string policyname { get; set; }

        [StringLength(250)]
        [DisplayName("Policy Desc:")]
        public string policydesc { get; set; }
        [DisplayName("Policy Amount :")]
        public decimal? amount { get; set; }
        [DisplayName("Policy EMI :")]
        public decimal? emi { get; set; }
        [DisplayName("Company Id :")]
        public int companyId { get; set; }

        [StringLength(50)]
        [DisplayName("Medical Id :")]
        public string medicalid { get; set; }

        public virtual Company Company { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PolicyRequest> PolicyRequests { get; set; }
    }
}
