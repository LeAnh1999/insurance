﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InsuranceT.Models;

namespace InsuranceT.Controllers
{
    public class EmployeesController : Controller
    {
        private Model1 db = new Model1();

        // GET: Employees
        public ActionResult Index()
        {
            return View(db.Employees.ToList());
        }

        // GET: Employees/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: Employees/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,designation,joindate,salary,firstname,lastname,username,password,address,contactno,state,country,city")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Employees.Add(employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(employee);
        }

        // GET: Employees/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,designation,joindate,salary,firstname,lastname,username,password,address,contactno,state,country,city")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(employee);
        }

        // GET: Employees/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employee = db.Employees.Find(id);
            db.Employees.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(EmployeeLogin user)
        {
            using (Model1 db = new Model1())
            {
                var usr = db.Employees.Where(u => u.username == user.username && u.password == user.password).SingleOrDefault();
                if (usr != null)
                {
                    //   Session["UserID"] = user.UserID;
                    Session["EnployeeId"] = usr.Id.ToString();
                    return RedirectToAction("LoggedIn");
                }
                else
                {
                    ModelState.AddModelError("", "Username or Password is wrong.");
                }
            }
            return View();
        }
        public ActionResult LoggedIn()
        {
            if (Session["EnployeeId"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }
        public ActionResult PolicyDetails()
        {

            var policies = db.policies.Include(p => p.Company);
            return View(policies.ToList());
        }
        public ActionResult PolicyDetailsa(int? id)
        {
            Session["PolicyId"] = id;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            policy policy = db.policies.Find(id);
            if (policy == null)
            {
                return HttpNotFound();
            }
            IList<policy> listpolicy = new List<policy>();
            listpolicy.Add(policy);

            ViewData["policy"] = listpolicy;
            var policies = db.policies.Include(p => p.Company);
            return View(policies.ToList());
        }
        public ActionResult Order()
        {
            var id = Session["PolicyId"];

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            policy policy = db.policies.Find(id);
            if (policy == null)
            {
                return HttpNotFound();
            }
            var order = new RequestDetail()
            {

                Amount = policy.amount,
                CompanyId = policy.companyId,
                CompanyName = policy.Company.CompanyName,
                Emi = policy.emi,
                RequestDate = DateTime.Now,
                Id = policy.Id,
                Name = policy.Company.CompanyName


            };
            return View(order);
        }
        public ActionResult RequestList()
        {
            int EmployeeId = Convert.ToInt32(Session["EnployeeId"]);
            int PolicyId = Convert.ToInt32(Session["PolicyId"]);
            var PolicyRequest = new PolicyRequest()
            {
                EmployeeId = EmployeeId,
                PolicyId = PolicyId,
                RequestDate = DateTime.Now,
                Status = "Not Approval"

            };

            db.PolicyRequests.Add(PolicyRequest);
            db.SaveChanges();
            List<PolicyRequest> usr = db.PolicyRequests.Where(u => u.EmployeeId == EmployeeId).ToList();
            return View(usr);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
